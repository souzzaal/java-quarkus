package br.com.souzzaal;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class UserResourceTest {

    @Test
    public void testGet() {
        given()
                .when().get("/users")
                .then()
                .statusCode(200);
    }

    @Test
    public void testGetOne() {
        User user = new User();
        user.name = "Andre";
        user.email = "email@gmail.com";

        int userId = given()
                .contentType("application/json")
                .body(user)
                .when().post("/users").then()
                .extract().path("id");

        given().pathParam("id", userId)
                .when().get("/users/{id}").then()
                .statusCode(200);
    }

    @Test
    public void testPost() {
        User user = new User();
        user.name = "Andre";
        user.email = "email@gmail.com";

        given()
                .contentType("application/json")
                .body(user)
                .when().post("/users")
                .then()
                .statusCode(201);
    }

    @Test
    public void testPut() {
        User user = new User();
        user.name = "Andre";
        user.email = "email@gmail.com";

        int userId = given()
                .contentType("application/json")
                .body(user)
                .when().post("/users").then()
                .extract().path("id");

        given().pathParam("id", userId)
                .contentType("application/json")
                .body(user)
                .when().put("/users/{id}").then()
                .statusCode(200);
    }

    @Test
    public void testDelete() {
        User user = new User();
        user.name = "Andre";
        user.email = "email@gmail.com";

        int userId = given()
                .contentType("application/json")
                .body(user)
                .when().post("/users").then()
                .extract().path("id");

        given().pathParam("id", userId)
                .when().delete("/users/{id}").then()
                .statusCode(200);
    }

}
