package br.com.souzzaal;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class User extends PanacheEntity {

    @NotNull
    @Column
    public String name;

    @NotNull
    @Column
    public String email;
}
