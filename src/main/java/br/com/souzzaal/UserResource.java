package br.com.souzzaal;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

@Path("/users")
public class UserResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response findAll() {
        return Response.ok(User.findAll().list()).build();
    }

    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response findOne(@PathParam("id") Long id) {
        return Response.ok(User.findById(id)).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)   // recebe um json
    @Produces(MediaType.APPLICATION_JSON)   // retorna um json
    @Transactional // Abre uma transacao antes de persistir: realiza commit para sucesso ou o rollback para erro
    public Response save(@Valid User user) throws URISyntaxException {
        user.persist();
        return Response.created(new URI("/users/" + user.id)).entity(user).build();
    }


    @Path("{id}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)   // recebe um json
    @Produces(MediaType.APPLICATION_JSON)   // retorna um json
    @Transactional // Abre uma transacao antes de persistir: realiza commit para sucesso ou o rollback para erro
    public Response update(@PathParam("id") Long id, @Valid User user) {

        User userDB = User.findById(id);
        userDB.name = user.name;
        userDB.email = user.email;
        userDB.persist();

        return Response.ok(userDB).build();
    }

    @Path("/{id}")
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)   // recebe um json
    @Produces(MediaType.APPLICATION_JSON)   // retorna um json
    @Transactional // Abre uma transacao antes de persistir: realiza commit para sucesso ou o rollback para erro
    public Response delete(@PathParam("id") Long id) {
        User.deleteById(id);
        return Response.ok(new User()).build();
    }
}
