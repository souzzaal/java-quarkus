# Quarkus - Supersonic Subatomic Java

## Problemática

Atualmente, com o uso de containers entrega-se a aplicação junto com o ambiente em  que ela ira executar. Ou seja, ao enviar uma aplicação para produção enviar-se o ambiente completo. O que representa mais controle o ambiente que executara a aplicação.

Com o avanço da computação nas nuvens - IaaS (Infrastructure as a Service) - e a adoção de microsserviços, servless e FaaS (Functions as a Service) tempo de processamento (CPU) e memoria passaram a ser recursos custosos. 
Do ponto de vista da computação na nuvem (microsservicos, conteinerização) Java não é muito adequado visto que aplicações, mesmo com frameworks mais modernos, como o Spring, ainda consomem muita CPU e memoria. (Exemplo: o tempo para levantar uma  aplicação Spring, por exemplo, é muito maior que o tempo necessário para levantar uma aplicação NodeJS)

Visando tornar a linguagem Java mais prepara para este novo cenário, a Red Hat lançou o Quarkus. 

## O que é?

É um projeto open-source, focando no Kubernetes, visando adaptar o ecossistema Java para atender novas necessidades, como containers cold start e processos de curta duração, entregando aplicações com baixo consumo de memória e tempo de resposta.

Caraterísticas especiais do Quarkus:

- Build AoT (Ahead of Time)
  - Realiza otimização durante o build visando gerar um código "pronto"
    - Ex: substitui todas as injeções de dependência (@Autowired) por instanciação de objetos, assim nada precisa ser feito em tempo  de execução.
    - Ex: exclusão de código morto (libs não utilizadas)
    - Ex: Minificação de código
- GraalVM Native (Oracle)
  - Usado para gerar um artefato nativo para o sistema operacional que ira executar a aplicação. 
    - A aplicação roda mais sobre a JVM
- Ecosystem as plugin
  - Todo o ecossistema da Plataforma Java como extensões (também otimizadas visando baixo consumo de memoria e tempo de execução)
- Developer Joy
  - Funcionalidades como hot reload (tipo o nodemon)
- Container first
  - Foi pensado uso com Kubernates
    - Para explorar microsservicos, nanosservicos, funções como serviço.

## Primeiros passos

1. Obter/criar projeto
   1. Método 1: acessar o site https://code.quarkus.io, preencher os dados, selecionar as dependências e baixar o zip. 
   2. Método 2: é possível criar um projeto via linha de comando com maven ou gradle.
2. Compilar o projeto mvn compile    
3. Levantar um servidor local para desenvolvimento:`./mvnw quarkus:dev` (na raiz do projeto)
4. Acessar a aplicação em `http://localhost:8080`

Ao final do desenvolvimento, para gerar o artefato a ser implantado execute:

- `mvn clean compile package`
  - produz o artefato target/<application-name><version>-runner.jar  (Ex: java-quarkus-1.0.0-SNAPSHOT-runner.jar)
    - não é fat/uberJar
    - executa sobre JVM (é como se fosse uma aplicação tradicional Java)
    - para executar: `java -jar /target/java-quarkus-1.0.0-SNAPSHOT-runner.jar`
- `mvn clean compile package -Pnative`
  - produz artefato nativo para o ambiente em que o comando foi executado, 
    - contem tudo o que for necessário para a aplicação rodar
    - para executar: `./target/java-quarkus-1.0.0-SNAPSHOT-runner`
- `mvn package -Pnative -Dquarkus.native.container-build=true`
  - produz artefato para Linux
  - ver src/docker/Dockerfile.native

## Referências

Documento baseado em:

- Wesley Fuchter: https://www.youtube.com/watch?v=S-L-ZT4SA7Q
- Quarkus: https://quarkus.io

