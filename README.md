# Projeto Java Quarkus

Projeto, para estudo do [Quarkus](https://quarkus.io/), no qual foi desenvolvida uma aplicação simplista para CRUD de usuários através de uma API Rest.

Tecnologias utilizadas:

- Java
  
- Quarkus
  
- H2 Database Engine
  

## Executando a aplicação em modo de desenvolvimento

Levanta um servidor local que permite *live coding*:

```
./mvnw quarkus:dev  
```

## Criando um executável nativo

Para o SO em que se está desenvolvendo: `./mvnw package -Pnative`.

De qualquer SO para Linux: `./mvnw package -Pnative -Dquarkus.native.container-build=true`.

Para executar a aplicação nativa :`./target/java-quarkus-1.0.0-SNAPSHOT-runner`